import { Storage } from "aws-amplify";
import { s3Upload } from "../libs/awsLib";
import { API } from "aws-amplify";
import mockResponse from "./mock/homepage";


class ProductApi {
    static create(params) {
        // title: this.state.title,
        // description: this.state.description,
        // fullImage: this.file,
        var sendParams = {
            title: params.title, 
            description: params.description,
            defaultImage: params.defaultImage
        }


        return API.post("bad-ideas", "/ideas", {body: sendParams}).then( response =>  {
                return response                
            }).catch(error =>{
                    return new Promise(error);
            })    
    }

    static getProducts(LastEvaluatedKey) {
        var sendParams = {};

        if (LastEvaluatedKey){
            sendParams = {body: {LastEvaluatedKey: LastEvaluatedKey}}

            return API.post("bad-ideas", "/products", sendParams).then( response =>  {
                return response
            }).catch(error =>{
                return new Promise(error);
            })
        } else {
            return API.get("bad-ideas", "/products")
                .then( response =>  {
                    return response
                }).catch(error =>{
                    return new Promise(error);
                })
        }



        //Mock Response for now
        // return new Promise( (res, rej) => res(mockResponse) );

    }

    static update(params) {
        // title: this.state.title,
        // description: this.state.description,
        // fullImage: this.file,
        var sendParams = {
            id:params.id,
            title: params.title,
            description: params.description,
            defaultImage: params.defaultImage
        }

        return API.put("bad-ideas", "/ideas/"+ sendParams.id, {body: sendParams}).then( response =>  {
            return response
        }).catch(error =>{
            return new Promise(error);
        })
    }
    
}
export default ProductApi;



//Example of how to use this file to make service calls

  //   const request = new Request(API_URL+'signin', {
    //     method: 'POST',
    //     headers: new Headers({
    //       'Content-Type': 'application/json'
    //     }),
    //     body: JSON.stringify({login: credentials})
    //   });
  
    //   return fetch(request)
    //     .then(response => response.json())
    //     .catch(error => {
    //       return error;
    //     })