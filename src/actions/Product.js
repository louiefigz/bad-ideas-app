import ProductApi from './ProductApi';



export function createProduct(params) {
    return (dispatch)=> {
        var fullImage = params.fullImage;

        return ProductApi.create(params)
            .then(response => {
                dispatch({
                    type: 'CREATE_PRODUCT_SUCCESS',
                    payload: response
                })
            }).catch(error =>{
                //  new Promise(error);
                return new Promise(error.message)
            })
    }
}

export function getProducts(LastEvaluatedKey) {
    return (dispatch)=> {
        // var fullImage = params.fullImage;

        return ProductApi.getProducts(LastEvaluatedKey)
            .then(response => {
                dispatch({
                    type: 'GET_ALL_PRODUCTS_SUCCESS',
                    payload: response
                })
            }).catch(error =>{
                //  new Promise(error);
                return new Promise(error.message)
            })
    }
}

export function updateProduct(params) {
    return (dispatch)=> {
        var fullImage = params.fullImage;

        return ProductApi.update(params)
            .then(response => {
                dispatch({
                    type: 'UPDATE_PRODUCT_SUCCESS',
                    payload: response
                })
            }).catch(error =>{
                //  new Promise(error);
                return new Promise(error.message)
            })
    }
}





  // This page is meant to handle the Logic.



    //   return UserApi.signin(credentials)
        // .then(response => {
        //   if(response.jwt) {
        //     localStorage.setItem("jwt", response.jwt);
        //     localStorage.setItem("user", JSON.stringify({
        //       id: response.id, email: response.email, name: response.name,
        //     }));
        //     history.push(redirect)
        //     dispatch({
        //       type: 'SUCCESS',
        //       // response = {jwt: KEY, id: INT, email: STR, name: STR}
        //       payload: response,
        //     })
        //   }
        //   else {
        //     dispatch({
        //       type: 'FAILURE',
        //       //response = {"error":"Invalid username or password"}
        //       payload: response.errors,
        //       //this.props.user.error
        //     })
        //   }
        // })
        // .catch(error => {
        //   throw(error);
        // })
