import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import CreateProduct from '../components/Product/create';
import ShowProduct from '../components/Product/show';
import AllProducts from '../components/Product/allProducts';
import SigninRoutes from './SigninRoutes';
import List from './list';

//Add All Routes Below

const AutoRedirect = () => <Route render={() => <Redirect to="/" />} />;


export default ({ childProps }) =>

        <Switch>
            <Route exact path='/' render={(childProps) => <AllProducts {...childProps} />}/>
            <Route exact path='/create' render={(childProps) => <CreateProduct {...childProps}  />}/>
            <Route exact path='/list' render={(childProps) => <List {...childProps}  />}/>
            <Route exact path='/product/:id' render={(childProps) => <ShowProduct {...childProps}  />}/>
            <Route exact path='/products' render={(childProps) => <AllProducts {...childProps} />}/>
            <SigninRoutes childProps={childProps}/>

            <AutoRedirect/>
        </Switch>;
