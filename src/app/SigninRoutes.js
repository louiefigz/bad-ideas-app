import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import Login from '../components/Auth/login/Login';
import Verify from '../components/Auth/verify/verify';
import Signup from '../components/Auth/signup/Signup';

//Add All Routes Below

export default ({ childProps }) =>
  <Switch>
    <Route path="/login" exact component={Login} props={childProps} />
    <Route path="/verify"  component={Verify} props={childProps} />
    <Route path="/signup" exact component={Signup} props={childProps} />
  </Switch>;
