import React, { Component } from 'react';
import  Routes  from './Routes';
import Header from '../components/header/Header';



export default class App extends Component {

  render() {

      return (
        <div>
          <Header/>
            <Routes childProps={this.props}/>
        </div>
      )
  }
}



