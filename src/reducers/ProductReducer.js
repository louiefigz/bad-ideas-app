const initialState = {
    title: false,
    description: null,
    error: "", 
    success: false, 
    rating: 0,
    allProducts: [],
    LastEvaluatedKey: null
  };

function ProductReducer(state = initialState, action) {
      switch(action.type){
        case 'CREATE_PRODUCT_SUCCESS':

          return state

        case 'UPDATE_PRODUCT_SUCCESS':
            return state

        case 'GET_ALL_PRODUCTS_SUCCESS':
          return {...state,
              allProducts: state.allProducts.concat(action.payload.Items),
              LastEvaluatedKey: action.payload.LastEvaluatedKey
          }
        
        default:
          return state;
      }
  }
  
  export default ProductReducer;