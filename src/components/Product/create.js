import React, { Component } from "react";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import config from "../../config";
import { s3Upload } from "../../libs/awsLib";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createProduct } from '../../actions/Product';
import { withRouter } from 'react-router';



class CreateProduct extends Component {
    constructor(props) {
        super(props);

        this.file = null;
        this.state = {
            isLoading: null,
            title: "",
            description: "", 
            loadMessage: ""
        };
    }

    validateForm() {
        return this.state.content.length > 0;
    }

    handleChange = event => {
      
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleFileChange = event => {
        this.file = event.target.files[0];
    }


  handleSubmit = async event => {
    event.preventDefault();

    if (this.file && this.file.size > config.MAX_ATTACHMENT_SIZE) {
      alert(`Please pick a file smaller than ${config.MAX_ATTACHMENT_SIZE/1000000} MB.`);
      return;
    }
    this.setState({ 
      isLoading: true,
      loadMessage: "Submitting in Progress" 
    });

    try {
      const defaultImage = this.file
        ? await s3Upload(this.file)
        : null;

        await this.props.createProduct({
          title: this.state.title,
          description: this.state.description,
          fullImage: this.file,
          defaultImage
        });
      this.props.history.push("/");
    } catch (e) {
      alert(e.message);
      this.setState({ isLoading: false, loadMessage: e.message });
    }
  }

  
    render() {
        return (

          this.state.isLoading ? 
            <div>
              {this.state.loadMessage}
            </div>
          :
            <div className="create-product-form">
              <h1>Title of Idea</h1>
              <textarea
                id="title"
                onChange={this.handleChange}
                value={this.state.title}
              />

              <h1>description</h1>
              <textarea
                id="description"
                onChange={this.handleChange}
                value={this.state.description}
              />

              <h1>Attachment</h1>

              <FormControl onChange={this.handleFileChange} type="file" />

              <div onChange={this.handleFileChange} type="file"></div>
              <button onClick={this.handleSubmit}>Submit</button>

            </div>
        );
    }
}

    const mapStateToProps = (state) => {
        return {
            session: state.user.session
        }
    }

    const mapDispatchToProps = (dispatch) => {
        return bindActionCreators({
            createProduct: createProduct
        }, dispatch)
    }

    export default withRouter(
        connect(mapStateToProps, mapDispatchToProps)(CreateProduct)
    );
