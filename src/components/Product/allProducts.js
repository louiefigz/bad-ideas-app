import React, { Component } from "react";
import { API, Storage } from "aws-amplify";
import DisplayItems from "./displayItems";
import {bindActionCreators} from "redux";
import {getProducts} from "../../actions/Product";
import {connect} from "react-redux";
import { withRouter } from 'react-router';



 class AllProducts extends Component {

   componentDidMount() {
   try {
          this.props.getProducts();

    } catch (e) {
      alert(e);
    }
  };


 nextPage = async event =>  {
    event.preventDefault();

    try {

        if (this.props.LastEvaluatedKey) {
            this.props.getProducts(this.props.LastEvaluatedKey);
        }

      } catch (e) {
        alert(e);
      }
    
  };

  render() {
    return (
      <div className="showProduct">

          {
              this.props.allProducts.map( (product)=> <DisplayItems key={product.product_id} item={product}/> )
          }

          {
              this.props.LastEvaluatedKey ? <button onClick={this.nextPage}> Next Item please </button> :  <h1>No More Products</h1>
          }

      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        allProducts: state.product.allProducts,
        LastEvaluatedKey: state.product.LastEvaluatedKey
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getProducts: getProducts
    }, dispatch)
};

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(AllProducts)
);
