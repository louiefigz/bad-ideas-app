import React, { Component } from "react";
import { API, Storage } from "aws-amplify";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import config from "../../config";
import {s3Upload} from "../../libs/awsLib";
import {createProduct, updateProduct} from '../../actions/Product';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';




 class UpdateProduct extends Component {
    constructor(props) {
        super(props);

        this.state ={
            defaultImage: '',
            description: '',
            product_id: '',
            title: ''
        }
    }


    handleChange = event => {

        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleFileChange = event => {
        this.file = event.target.files[0];
    }

    handleSubmit = async event => {
        event.preventDefault();

        if (this.file && this.file.size > config.MAX_ATTACHMENT_SIZE) {
            alert(`Please pick a file smaller than ${config.MAX_ATTACHMENT_SIZE/1000000} MB.`);
            return;
        }
        this.setState({
            isLoading: true,
            loadMessage: "Submitting in Progress"
        });

        try {
            const defaultImage = this.file
                ? await s3Upload(this.file)
                : null;

            await this.props.updateProduct({
                id: this.props.match.params.id,
                title: this.state.title,
                description: this.state.description,
                fullImage: this.file,
                defaultImage
            });
            this.props.history.push("/");
        } catch (e) {
            alert(e.message);
            this.setState({ isLoading: false, loadMessage: e.message });
        }
    }

    componentWillMount(){

        const {title, product_id, description, defaultImage} = this.props.state.item;

        this.setState({
            title: title,
            description: description,
            defaultImage: defaultImage,
            product_id: product_id
        })
    }





    render() {
        return (
            <div className="showProduct">

                <div className="create-product-form">

                    <h1>THIS IS THE EDIT PAGE </h1>

                    <h1>Title of Idea</h1>
                    <textarea
                        id="title"
                        onChange={this.handleChange}
                        value={this.state.title}
                    />

                    <h1>description</h1>
                    <textarea
                        id="description"
                        onChange={this.handleChange}
                        value={this.state.description}
                    />

                    <h1>Attachment</h1>

                    <FormControl onChange={this.handleFileChange} type="file" />

                    <div onChange={this.handleFileChange} type="file"></div>
                    <button onClick={this.handleSubmit}>Submit</button>

                </div>


            </div>


        )
    }
}

const mapStateToProps = (state) => {
    return {
        session: state.user.session
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        updateProduct: updateProduct
    }, dispatch)
}

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(UpdateProduct)
);
