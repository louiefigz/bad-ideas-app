import React, { Component } from "react";
import UpdateProduct from "./update";
import { API, Storage } from "aws-amplify";

export default class showProduct extends Component {
  constructor(props) {
    super(props);

    this.state ={
      defaultImage: '', 
      description: '', 
      product_id: '', 
      title: '',
      edit: false
    }
    
  }

  async componentDidMount() {
    try {
      const product = await this.getNote();
      const { defaultImage, description, product_id, title } = product;

      if (defaultImage) {
        var defaultImageUrl = await Storage.vault.get(defaultImage);
      }

      this.setState({
        defaultImage: defaultImageUrl, 
        description: description, 
        product_id: product_id, 
        title: title
      });

    } catch (e) {
      alert(e);
    }
  }

  editClicked(){
    this.setState({
        edit: true
    });
  }

  getNote() {
    return API.get("bad-ideas", `/product/${this.props.match.params.id}`);
  }

  render() {
    return (
      <div className="showProduct">
          



          {this.state.edit ?

              <UpdateProduct state={this.state}/>
              :
              <div>
                <h1>Title</h1>
                  {this.state.title}
                <h1>Description</h1>
                  {this.state.description}
                <h1>Image</h1>
                  <img src={this.state.defaultImage}></img>

                <button onClick={this.editClicked.bind(this)}>Edit</button>
              </div>
          }

      </div>
    )
  }
}