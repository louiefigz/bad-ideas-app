import React, { Component } from "react";
import UpdateProduct from "./update";


export default class DisplayItems extends Component {
    constructor (){
        super();
        this.state = {
            edit: false
        }
    }

    editClicked(){
        this.setState({
            edit: !this.state.edit
        });
    }


    render(){
        const {user_id, likes, createdAt, description, defaultImage, product_id, title} = this.props.item;
        return(
            <section>
                {
                    this.state.edit ?
                        <UpdateProduct state={this.props} />

                    :
                        <article>
                        +++++++++++++++++++ ITEM ++++++++++++++++++
                            <div className="idea-wrap">
                                <h1>Title</h1>
                                    {title}
                                <h2>Description</h2>
                                    {description}
                                <h2>created At</h2>
                                    {createdAt}
                                <h2>likes</h2>
                                    {likes}
                                <h2>Image</h2>
                                <img src={defaultImage}></img>
                            </div>
                        </article>
                }

                <button onClick={this.editClicked.bind(this)}>{this.state.edit ? "Done" : "Edit"}</button>

            </section>
        )
    }

}