import React, { Component } from "react";
import {
  HelpBlock,
  FormGroup,
  FormControl,
  ControlLabel
} from "react-bootstrap";

import { bindActionCreators } from 'redux';

import { connect } from 'react-redux';
import { signUpUser,loginUser, confirmSignup } from '../../../actions/Auth';
import  IsPassValid  from '../lib/passwordValidation';
import { withRouter } from 'react-router';




class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
        isLoading: false,
        email: "",
        password: "",
        confirmPassword: "",
        confirmationCode: "",
        NumValidMessage: <div>password must have atleast one number</div>,
        upperValidMessage: <div>password must have atleast one uppercase letter</div>,
        confirmPassMessage: <div>Your passwords do not match</div>,
        charsLengthValidMessage: <div>password must be atleast 9 characters.</div>,
        backEndError: "",
        isUpperValid: false,
        isNumValid: false,
        isCharsValid: false,
        isConfirmValid: false,
        newUser: null
    };
  }

    validateForm() {
        return (
            this.state.isUpperValid &&
            this.state.isNumValid  &&
            this.state.isCharLengthValid &&
            this.state.isConfirmValid
        );
    }


    IsValid(value) {

        var validResults = IsPassValid( value);

        this.setState({
            isUpperValid: validResults.isUpperValid,
            isNumValid: validResults.isNumValid,
            isCharLengthValid: validResults.isCharLengthValid
        })
    }

    IsConfirmValid=(confirm)=> {
        this.setState({
            isConfirmValid: this.state.password === confirm
        })
    }


  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.id === "email" ?  event.target.value.toLowerCase().replace(/\s/g, '') : event.target.value
    });

    if (event.target.id === 'password'){
        this.IsValid(event.target.value, );
    }

    if (event.target.id === 'confirmPassword'){
      this.IsConfirmValid(event.target.value);
    }
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    if ( this.validateForm() ) {

        try {
            const newUser = await this.props.signUpUser({
                username: this.state.email,
                password: this.state.password
            });
            this.setState({
                newUser
            });
        } catch (error) {
            this.setState({
              backEndError: error.message
            });
        }
    } else {
        alert('Please enter a valid password');
    }

    this.setState({ isLoading: false });
  }

  handleConfirmationSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await this.props.confirmSignup(this.state);
      await this.props.loginUser(this.state, this.props.history, '/');

     /* this.props.userHasAuthenticated(true);
      this.props.history.push("/");*/
    } catch (e) {
      alert(e.message);
      this.setState({ isLoading: false });
    }
  }

  renderConfirmationForm() {
    return (
      <form onSubmit={this.handleConfirmationSubmit}>
        <FormGroup controlId="confirmationCode" bsSize="large">
          <ControlLabel>Confirmation Code</ControlLabel>
          <FormControl
            autoFocus
            type="tel"
            value={this.state.confirmationCode}
            onChange={this.handleChange}
          />
          <HelpBlock>Please check your email for the code.</HelpBlock>
        </FormGroup>

      </form>
    );
  }

  ValidationMessages() {
      return (
          <div>
              {this.state.isUpperValid  ?  null : this.state.upperValidMessage}
              {this.state.isNumValid ? null : this.state.NumValidMessage}
              {this.state.isCharLengthValid ? null : this.state.charsLengthValidMessage}
              {this.state.isConfirmValid ? null : this.state.confirmPassMessage}
              {this.state.backEndError}
          </div>
      )
  }

  renderForm() {
    return (
      <form>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            autoFocus
            type="email"
            value={this.state.email}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>Password</ControlLabel>
          <FormControl
            value={this.state.password}
            onChange={this.handleChange}
            type="password"
          />
        </FormGroup>
        <FormGroup controlId="confirmPassword" bsSize="large">
          <ControlLabel>Confirm Password</ControlLabel>
          <FormControl
            value={this.state.confirmPassword}
            onChange={this.handleChange}
            type="password"
          />
        </FormGroup>

          {this.ValidationMessages()}

        <button onClick={this.handleSubmit}>Submit</button>

      </form>
    );
  }

  render() {
    return (
      <div className="Signup">
        {this.state.newUser === null
          ? this.renderForm()
          : this.renderConfirmationForm()}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        signUpUser: signUpUser,
        confirmSignup:confirmSignup,
        loginUser:loginUser }, dispatch)
};

export default withRouter(
    connect(null, mapDispatchToProps)(Signup)
);
