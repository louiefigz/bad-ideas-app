
   const  IsPassValid = (value) => {

        var isUpperValid = false,
            isNumValid = false,
            isCharLengthValid = false;


        //is at least 10 characters long
        if (value.length >= 9){
            isCharLengthValid = true;
        }

        //Has atleast 1 uppercase letter.
        value.split('').forEach( (letter) =>{
            if (/[A-Z]/.test(letter)){
                isUpperValid = true
            }

            if(/[0-9]/.test(letter)){
                isNumValid = true;
            }
        });


        return {
            isUpperValid: isUpperValid,
            isNumValid: isNumValid,
            isCharLengthValid: isCharLengthValid
        }

    }

    export default IsPassValid;
