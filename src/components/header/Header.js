import React, {Component, Fragment} from 'react';
import '../../app/dist/header.css';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import {  NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { loginUser, logOut, currentUser } from '../../actions/Auth';
import { bindActionCreators } from 'redux';



class Header extends Component {

    handleLogout() {
        // this.userHasAuthenticated(false);
        this.props.logOut(this.props.history, '/login');
      }

    render() {
        return (
            <div className="header">
                <h1>Bad Ideas</h1>
                <div className="login">
                    <ul>
                        {
                        this.props.session ?
                            <NavItem onClick={this.handleLogout.bind(this)}>Logout</NavItem>
                            :
                            <Fragment>
                                <LinkContainer to="/signup">
                                    <NavItem>Signup</NavItem>
                                </LinkContainer>
                                <LinkContainer to="/login">
                                    <NavItem>Login</NavItem>
                                </LinkContainer>
                            </Fragment>
                        }
                    </ul>
                    <LinkContainer to="/">
                        <NavItem>Home</NavItem>
                    </LinkContainer>
               </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
      session: state.user.session
    }
  };
  const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        loginUser: loginUser,
        logOut: logOut,
        currentUser: currentUser
      }, dispatch)
  };
   export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(Header)
  );
