import uuid from "uuid";
import AWS from "aws-sdk";
AWS.config.update({ region: "us-east-1" });
const dynamoDb = new AWS.DynamoDB.DocumentClient();

// export async function main(event, context, callback) {
//   const params = {
//     TableName: "notes",
//     // 'KeyConditionExpression' defines the condition for the query
//     // - 'userId = :userId': only return items with matching 'userId'
//     //   partition key
//     // 'ExpressionAttributeValues' defines the value in the condition
//     // - ':userId': defines 'userId' to be Identity Pool identity id
//     //   of the authenticated user
//     KeyConditionExpression: "userId = :userId",
//     ExpressionAttributeValues: {
//       ":userId": event.requestContext.identity.cognitoIdentityId
//     }
//   };
//
//   try {
//     const result = await dynamoDbLib.call("query", params);
//     // Return the matching list of items in response body
//     callback(null, success(result.Items));
//   } catch (e) {
//     callback(null, failure({ status: false }));
//   }
// }

export function main(event, context, callback) {
    // Request body is passed in as a JSON encoded string in 'event.body'

    const params = {
        TableName: "bad-notes",
        // 'Item' contains the attributes of the item to be created
        // - 'userId': user identities are federated through the
        //             Cognito Identity Pool, we will use the identity id
        //             as the user id of the authenticated user
        // - 'noteId': a unique uuid
        // - 'content': parsed from request body
        // - 'attachment': parsed from request body
        // - 'createdAt': current Unix timestamp
        KeyConditionExpression: "userid = :userid",
        ExpressionAttributeValues: {
          ":userid": event.requestContext.identity.cognitoIdentityId
        }
    };

    dynamoDb.query(params, (error, data) => {
        // Set response headers to enable CORS (Cross-Origin Resource Sharing)
        const headers = {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true
        };

        // Return status code 500 on error
        if (error) {
            const response = {
                statusCode: 500,
                headers: headers,
                body: JSON.stringify({ status: false })
            };
            callback(null, response);
            return;
        }

        // Return status code 200 and the newly created item
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(data.Items)
        };
        callback(null, response);
    });
}
