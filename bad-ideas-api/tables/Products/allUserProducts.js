import AWS from "aws-sdk";

AWS.config.update({ region: "us-east-1" });
const dynamoDb = new AWS.DynamoDB.DocumentClient();


export function main(event, context, callback) {
    // Request body is passed in as a JSON encoded string in 'event.body'
    const params = {
        TableName: "Products",
        Limit: 15
    };

    if (event && event.body) {
        params.ExclusiveStartKey = JSON.parse(event.body).LastEvaluatedKey;
    }

    dynamoDb.scan(params, (error, data) => {
        // Set response headers to enable CORS (Cross-Origin Resource Sharing)
        const headers = {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true
        };

        // Return status code 500 on error
        if (error) {
            const response = {
                statusCode: 500,
                headers: headers,
                body: JSON.stringify({ status: false })
            };
            callback(null, response);
            return;
        }

        // Return status code 200 and the newly created item
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(data)
        };
        callback(null, response);
    });
}
