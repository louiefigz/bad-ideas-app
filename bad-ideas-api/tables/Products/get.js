// import * as dynamoDbLib from "./libs/dynamodb-lib";
// import { success, failure } from "./libs/response-libs";
import AWS from "aws-sdk";

AWS.config.update({ region: "us-east-1" });
const dynamoDb = new AWS.DynamoDB.DocumentClient();


// Test with the following 'serverless invoke local --function get --path mocks/get-event.json'


export function main(event, context, callback) {
    // Request body is passed in as a JSON encoded string in 'event.body'
    const data = JSON.parse(event.body);

    const params = {
        TableName: "Products",
        Key: {
            user_id: event.requestContext.identity.cognitoIdentityId,
            product_id: event.pathParameters.id
        }
    };

    dynamoDb.get(params, (error, data) => {
        // Set response headers to enable CORS (Cross-Origin Resource Sharing)
        const headers = {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true
        };

        // Return status code 500 on error
        if (error) {
            const response = {
                statusCode: 500,
                headers: headers,
                body: JSON.stringify({ status: false })
            };
            callback(null, response);
            return;
        }

        // Return status code 200 and the newly created item
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(data.Item)
        };
        callback(null, response);
    });
}


// export async function main(event, context, callback) {
//     const params = {
//         TableName: "bad-notes",
//         // 'Key' defines the partition key and sort key of the item to be retrieved
//         // - 'userId': Identity Pool identity id of the authenticated user
//         // - 'noteId': path parameter
//         Key: {
//             userId: event.requestContext.identity.cognitoIdentityId,
//             noteId: event.pathParameters.id
//         }
//     };
//
//     try {
//         const result = await dynamoDbLib.call("get", params);
//         if (result.Item) {
//             // Return the retrieved item
//             callback(null, success(result.Item));
//         } else {
//             callback(null, failure({ status: false, error: "Item not found." }));
//         }
//     } catch (e) {
//         callback(null, failure({ status: false }));
//     }
// }
