import uuid from "uuid";
import AWS from "aws-sdk";
// import * as dynamoDbLib from "./libs/dynamodb-lib";
// import { success, failure } from "./libs/response-libs";

AWS.config.update({ region: "us-east-1" });
const dynamoDb = new AWS.DynamoDB.DocumentClient();

// Test with the following 'serverless invoke local --function create --path mocks/create-event.json'


//
// export async function main(event, context, callback) {
//     const data = JSON.parse(event.body);
//     const params = {
//         TableName: "notes",
//         Item: {
//             userId: event.requestContext.identity.cognitoIdentityId,
//             noteId: uuid.v1(),
//             content: data.content,
//             attachment: data.attachment,
//             createdAt: Date.now()
//         }
//     };

//     try {
//         await dynamoDbLib.call("put", params);
//         callback(null, success(params.Item));
//     } catch (e) {
//         callback(null, failure({ status: false }));
//     }
// }




// //
export function main(event, context, callback) {
    // Request body is passed in as a JSON encoded string in 'event.body'
    const data = JSON.parse(event.body);

    const params = {
        TableName: "Products",
        Item: {
            user_id: event.requestContext.identity.cognitoIdentityId,
            product_id: uuid.v1(),
            title: data.title,
            description: data.description,
            defaultImage: data.defaultImage,
            likes: 0,
            createdAt: Date.now()
        }
    };

    dynamoDb.put(params, (error, data) => {
        // Set response headers to enable CORS (Cross-Origin Resource Sharing)
        const headers = {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true
        };

        // Return status code 500 on error
        if (error) {
            const response = {
                statusCode: 500,
                headers: headers,
                body: JSON.stringify({ status: false })
            };
            callback(null, response);
            return;
        }

        // Return status code 200 and the newly created item
        const response = {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(params.Item)
        };
        callback(null, response);
    });
}
